let botonCrear = document.getElementById('crear');
function crearForm() {

	// declarando variables para crear los elementos del formulario
	let contenedor = document.createElement('div');
	let titulo = document.createElement('h1');
	let textH1 = document.createTextNode('Formulario con el DOM'); 
	let newForm = document.createElement('form');
	let input1 = document.createElement('input');
	let input2 = document.createElement('input');
	let input3 = document.createElement('input');
	let label1 = document.createElement('p');
	let label2 = document.createElement('p');
	let simboloSuma = document.createTextNode('+');
	let simboloIgual = document.createTextNode('=');
	let boton = document.createElement('buttom');
	let nomBoton = document.createTextNode('Resultado');

	// agregando los atributos al div
	contenedor.setAttribute('class', 'contenido');
	// agregando el texto al H1
	titulo.appendChild(textH1);
	// agregando el H1 al DIV
	contenedor.appendChild(titulo);


	// primer imput
	input1.setAttribute('type', 'text');
	newForm.appendChild(input1);

	// simbolo suma
	label1.appendChild(simboloSuma);
	newForm.appendChild(label1);

	// segundo input
	input2.setAttribute('type', 'text');
	newForm.appendChild(input2);

	// simbolo igual
	label2.appendChild(simboloIgual);
	newForm.appendChild(label2);

	// tercer input desactivado
	input3.setAttribute('type', 'text');
	input3.setAttribute('readonly', true);
	newForm.appendChild(input3);

	// boton del resultado
	boton.appendChild(nomBoton);
	boton.setAttribute('class', 'btn');
	newForm.appendChild(boton);

	// agregamos el formulario al DIV
	contenedor.appendChild(newForm);

	// seleccionamos en que parte del documento html se agregará
	let form = document.getElementById('formulario');
	//agregamos al documento html
	form.appendChild(contenedor);
}

botonCrear.addEventListener('click', crearForm);
